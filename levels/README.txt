**Teleport**
(wenige Regeln)
Teleporter im Level, können alles nach oben teleportieren, so lange am
Ziel Platz ist

**GameOfLife**
(viele Regeln)
Game of Life mit Edelsteinen

**Wasserfall**
(benötigt Zufallserweiterung; mäßig viele Regeln)
Fließendes Wasser, löscht Feuer, wird zu Eis, Eis am Feuer wird zu
Edelsteinen

**Bomberman**
(benötigt Zufallserweiterung; viele Regeln)
Kombination aus Boulder Dash, Bomberman und den Geistern aus Pacman.
Bomben können mit Shift+Pfeiltaste gelegt werden.
