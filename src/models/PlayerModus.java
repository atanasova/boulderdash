package models;

/**
 * Author: Liudmila Kachurina (https://github.com/gracenotegracenote)
 * Date: 12-Feb-17
 */
public enum PlayerModus {
	SINGLEPLAYER, MULTIPLAYER
}
